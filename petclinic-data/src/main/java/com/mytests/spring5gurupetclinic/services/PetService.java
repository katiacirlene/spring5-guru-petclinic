package com.mytests.spring5gurupetclinic.services;

import com.mytests.spring5gurupetclinic.model.Pet;

public interface PetService extends CrudService<Pet, Long> {

}
