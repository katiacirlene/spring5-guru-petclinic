package com.mytests.spring5gurupetclinic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Spring5GuruPetclinicApplication {

    public static void main(String[] args) {
        SpringApplication.run(Spring5GuruPetclinicApplication.class, args);
    }
}
