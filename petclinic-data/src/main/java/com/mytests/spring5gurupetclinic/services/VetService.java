package com.mytests.spring5gurupetclinic.services;

import com.mytests.spring5gurupetclinic.model.Vet;

public interface VetService extends CrudService<Vet, Long> {

}
