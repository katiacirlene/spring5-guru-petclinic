package com.mytests.spring5gurupetclinic.services;

import com.mytests.spring5gurupetclinic.model.Owner;

public interface OwnerService extends CrudService<Owner, Long> {

    Owner findByLastName(String lastName);

}